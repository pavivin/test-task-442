from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from src.models import BaseModel

from sqlalchemy import Column, String


class Character(BaseModel):
    __tablename__ = "characters"

    user_id = Column(UUID, ForeignKey("users.id"), nullable=False)
    name = Column(String, nullable=False)
    level = Column(Integer, nullable=False, default=1)
    experience = Column(Integer, nullable=False, default=0)

    user = relationship("User", back_populates="characters")
    items = relationship("Item", back_populates="character")
