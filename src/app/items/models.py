from enum import Enum
from sqlalchemy import Column, String, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from src.models import BaseModel

from sqlalchemy import Column, String


class Rarity(Enum):
    COMMON = 1
    RARE = 2
    EPIC = 3


class Item(BaseModel):
    __tablename__ = "items"

    name = Column(String, nullable=False)
    type = Column(String, nullable=False)
    rarity = Column(String, nullable=False)
    character_id = Column(UUID, ForeignKey("characters.id"), nullable=True)

    character = relationship("Character", back_populates="items")
