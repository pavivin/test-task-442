import uuid
from fastapi import APIRouter, Depends
from taskiq import TaskiqResult

from src.app.auth.models import User
from src.app.items.schemas import DropItemRequest
from src.broker import rabbit
from src.broker.tasks.drop import drop_item_task
from src.auth.manager import current_active_user

router = APIRouter()


@router.post("/items/drop")
async def drop_item(request: DropItemRequest, user: User = Depends(current_active_user)):
    status = await drop_item_task.kiq(request=request, user_id=str(user.id))
    await rabbit.result_backend.set_result(task_id=status.task_id, result=TaskiqResult(is_err=False, log='In Progress', execution_time=0.0))
    return {"task_id": status.task_id}


@router.get("/task_status/{task_id}")
async def task_status(task_id: str):
    result = await rabbit.result_backend.get_result(task_id=task_id, with_logs=True)
    return {"status": result.log}
        
