import uuid
from src.app.core.protocol import BaseModel


class DropItemRequest(BaseModel):
    item_id: str # uuid.UUID There is some problem to convert UUID
    from_character_id:str
    to_character_id: str
