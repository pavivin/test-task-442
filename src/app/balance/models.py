from sqlalchemy import Column, Integer, String, ForeignKey, Numeric
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from src.models import BaseModel

from sqlalchemy import Column, String


class Balance(BaseModel):
    __tablename__ = "balances"

    user_id = Column(UUID, ForeignKey("users.id"), nullable=False)
    currency = Column(String, nullable=False)
    amount = Column(Numeric, nullable=False, default=0)

    user = relationship("User", back_populates="balances")
