from fastapi_users.db import SQLAlchemyBaseUserTableUUID

from src.db.base import Base

from sqlalchemy.orm import Mapped, relationship
from sqlalchemy import Column, String


class User(SQLAlchemyBaseUserTableUUID, Base):
    __tablename__ = "users"
    email: Mapped[str] = Column(String(length=254), unique=True)
    characters = relationship("Character", back_populates="user")
