from sqlalchemy import select
from taskiq import TaskiqResult
from src.app.auth.models import User
from src.app.items.schemas import DropItemRequest
from src.app.core.exceptions import CharacterNotFoundError, ItemNotFoundError
from src.app.character.models import Character
from src.app.items.models import Item
from src.broker import rabbit
from src.db.connection import Transaction


@rabbit.task(task_name="item.drop")
async def drop_item_task(request: DropItemRequest, user_id: str) -> int:
    async with Transaction() as session:
        to_character = await session.execute(
            select(Character).filter(Character.id == request.to_character_id, Character.user_id == user_id)
        )
        to_character = to_character.scalars().first()
        if not to_character:
            raise CharacterNotFoundError

        query = await session.execute(
            select(Item)
            .filter(Item.id == request.item_id, Item.character_id == request.from_character_id)
            .with_for_update(skip_locked=True)
        )
        item: Item = query.scalars().first()
        if not item:
            raise ItemNotFoundError

        item.character_id = request.to_character_id
        session.add(item)
        await session.commit()
        
        # await rabbit.result_backend.set_result(task_id=status.task_id, result=TaskiqResult(is_err=False, log='Complete', execution_time=0.0))
