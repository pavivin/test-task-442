from taskiq_aio_pika import AioPikaBroker
from taskiq_redis import RedisAsyncResultBackend

from src.config import settings

rabbit = AioPikaBroker(url=settings.BROKER_URL).with_result_backend(RedisAsyncResultBackend(settings.RESULT_BACKEND))
