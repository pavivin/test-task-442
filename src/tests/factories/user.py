import factory
from async_factory_boy.factory.sqlalchemy import AsyncSQLAlchemyFactory
from faker import Factory as FakerFactory
from pytest_factoryboy import register

from src.app.auth.models import User
from src.db.base import sc_session

faker = FakerFactory.create()


def get_password_hash(password: str):  # hardcode string result
    return "$argon2id$v=19$m=65536,t=3,p=4$CORWOrxkakqmkeYzSIwsAw$Uma4fAEKYTRkHwjHSc0cAwqmDHsoQsCa7PO+93hVm0o"


@register
class UserFactory(AsyncSQLAlchemyFactory):
    first_name = factory.LazyAttribute(lambda x: faker.name())
    last_name = factory.LazyAttribute(lambda x: faker.name())
    email = factory.LazyAttribute(lambda x: faker.email())
    hashed_password = factory.LazyAttribute(lambda x: get_password_hash("string"))

    class Meta:
        model = User
        sqlalchemy_session = sc_session
